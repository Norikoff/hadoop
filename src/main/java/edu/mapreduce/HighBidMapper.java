package edu.mapreduce;

import com.google.gson.Gson;
import edu.mapreduce.counters.AnyErrorCounter;
import edu.mapreduce.counters.BidNoFormatted;
import edu.mapreduce.dto.BidDto;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class HighBidMapper extends Mapper<Object, Text, Text, Text> {

    /**
     * Main delimiter character
     */
    private String DELIMETER = "\\s+"; // \s \t character

    /**
     * Helpful constant for private function bellow
     */
    private int STEP_BEHIND = 4;

    /**
     * Another helpful constant for private function bellow
     */
    private int CITY_ID  = 17;

    public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString();

        BidDto parsingResult = null;
        try {
            parsingResult = parseInputString(line);
        } catch (NumberFormatException ex) {
            context.getCounter(BidNoFormatted.BID_NO_FORMATTED_ERROR).increment(1);
            return;
        } catch (Exception ex) {
            context.getCounter(AnyErrorCounter.ANY_ERROR).increment(1);
            return;
        }
        if (parsingResult == null) {
            context.getCounter(BidNoFormatted.BID_NO_FORMATTED_ERROR).increment(1);
            return;
        }
        Gson gson = new Gson();
        String json = gson.toJson(parsingResult);
        // There is no reason to reuse Text variables as asked by task.
        // The variable can`t be initialize from the static context as a state without muting by mapper threads
        // The variable is initialize only 1 times
        context.write(new Text(parsingResult.getBidId()), new Text(json));
    }

    /**
     *
     * @param line - just a 1 row input line that was read by mapper job
     * Input String have \t and \s symbols as delimiter and have no escaped \s character
     * The main idea of parsing function find the last part by \s character
     * And step by constant elements from back
     * Constant is 4 for current scheme
     * @return formatted BidDto element
     */
    private BidDto parseInputString(String line) {
        String[] parsedBySpace = line.split(DELIMETER);
        return new BidDto(parsedBySpace[parsedBySpace.length - CITY_ID], Integer.parseInt(parsedBySpace[(parsedBySpace.length -1) - STEP_BEHIND]), 1);
    }
}
