package edu.mapreduce;

import com.google.gson.Gson;
import edu.mapreduce.dto.BidDto;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * агрегирует пары вида <Text,Text>
 * 0.0.0.0 -  {"requestCount":1,"sumLength":134,"avgLength":134.0}
 * где
 * requestCount количество запросов на данный ip
 * sumLength - суммарная длина всех запросов на данный ip
 * avgLength - средняя длина запросов на данный ip
 * <p>
 * Reduces суммируется requestCount и sumLength пар и вычисляет avgLength.
 */
public class HighBidReducer extends Reducer<Text, Text, Text, Text> {

    // magic constant should have persistent state with path in ./start-mr.sh script
    public static final String file = "/user/hadoop/input2/city.en";

    public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        //init reduce city map
        File initialFile = new File(file);
        InputStream targetStream = null;
        try {
            targetStream = new FileInputStream(initialFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Map<String, String> map = new HashMap<>();

        Scanner scanner = new Scanner(targetStream).useDelimiter("( |\n)");
        while (scanner.hasNext()) {
            map.put(scanner.next(), scanner.next());
        }

        Gson gson = new Gson();
        // Считается суммарное количество и суммарная длина запросов
        String concatIds = key.toString();
        int highBid = -1;
        int countOfHighBid = 0;
        for (Text val : values) {
            BidDto bidDto = gson.fromJson(val.toString(), BidDto.class);
            countOfHighBid += bidDto.getCountOfBidPrice();
            if (bidDto.getBidPrice() > highBid) {
                highBid = bidDto.getBidPrice();
                continue;
            }
            if (bidDto.getBidPrice() == highBid) {
                continue;
            }
        }
        // Вычисления записываются в json. avgLength рассчитывается как (double) sumLength / requestCount
        BidDto bidDto = new BidDto(concatIds, highBid, countOfHighBid);
        //Подмена cityId на cityName
        String cityName = map.get(bidDto.getBidId());
        if (cityName != null) {
            key.set(cityName);
            bidDto.setBidId(cityName);
        }
        String json = gson.toJson(bidDto);
        //результат передается дальше
        context.write(key, new Text(json));
    }

}