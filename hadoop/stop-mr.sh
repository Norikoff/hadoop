#/usr/bin/env bash

echo 'INFO: docker-compose -f cluster.yml stop && docker-compose -f cluster.yml rm -f'
docker-compose -f cluster.yml stop && docker-compose -f cluster.yml rm -f

exit 0;
